#include <stdio.h>
#include <openssl/bn.h>

void printBN(char *msg, BIGNUM * a){
	/* Use BN_bn2hex(a) for hex string
	* Use BN_bn2dec(a) for decimal string */
	char * number_str = BN_bn2hex(a);
	printf("%s %s\n", msg, number_str);
	OPENSSL_free(number_str);
}

int main ()
{
	/* Define variables */
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *p = BN_new();
	BIGNUM *q = BN_new();
	BIGNUM *e = BN_new();

	BIGNUM *p_minus_one = BN_new();
	BIGNUM *q_minus_one = BN_new();
	BIGNUM *totient = BN_new();

	BIGNUM *one = BN_new();

	BIGNUM *d = BN_new();

	BIGNUM *message = BN_new();
	BIGNUM *encrypted = BN_new();
	BIGNUM *decrypted = BN_new();
	BIGNUM *modulo = BN_new();

	/* Convert the given primes from hex string into
		Big number*/ 
	BN_hex2bn(&p, "F7E75FDC469067FFDC4E847C51F452DF");
	BN_hex2bn(&q, "E85CED54AF57E53E092113E62F436F4F");
	BN_hex2bn(&e, "0D88C3");

	BN_dec2bn(&one,"1");


	/* totient(n) = (p-1) * (q-1)*/
	BN_sub(p_minus_one, p, one);
	BN_sub(q_minus_one, q, one);
	BN_mul(totient, p_minus_one,q_minus_one, ctx);

	/* Calculate the value of d using mod inverse 
	   d = e^-1 mod totient(n)*/

	BN_mod_inverse(d, e, totient, ctx);

	printBN("Private Key:", d);

	BN_dec2bn(&message,"65");
	printBN("message: ", message);
	BN_mul(modulo, p,q, ctx);
	BN_mod_exp(encrypted,message,e,modulo,ctx);
	printBN("Encrypted: ",encrypted);

	BN_mod_exp(decrypted,encrypted,d,modulo,ctx);
	printBN("decrypted: ",decrypted);
	
	return 0;
}
