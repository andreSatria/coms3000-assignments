#include <stdio.h>
#include <openssl/bn.h>
#include <stdlib.h>
#include <string.h>

void printBN(char *msg, BIGNUM * a){
	/* Use BN_bn2hex(a) for hex string
	* Use BN_bn2dec(a) for decimal string */
	char * number_str = BN_bn2hex(a);
	printf("%s %s\n", msg, number_str);

	OPENSSL_free(number_str);
}


int main ()
{	
	/* Define variables*/
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *d = BN_new();
	BIGNUM *n = BN_new();
	BIGNUM *messageHex = BN_new();
	BIGNUM *messageHexTampered = BN_new();

	BIGNUM *difference = BN_new();

	BIGNUM *signature = BN_new();
	BIGNUM *tamperedSignature= BN_new();


	/*Convert the message hex into big num form */
	BN_hex2bn(&messageHex, "49206f776520796f752024323030302e");
	BN_hex2bn(&messageHexTampered,"49206f776520796f752024333030302e");
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");

	/* Check the difference in decimal form (for the hex)*/
	BN_sub(difference, messageHexTampered,messageHex);
	char * number_str = BN_bn2dec(difference);
	printf("Difference of value (decimal form): %s \n", number_str);

	/* Generate the signature for the original message*/
	BN_mod_exp(signature, messageHex, d, n, ctx);
	printBN("The signature for $2000 is: ", signature);

	/* Generate the other signature */
	BN_mod_exp(tamperedSignature, messageHexTampered, d, n, ctx);
	printBN("The signature for $3000 is: ", tamperedSignature);

	/* TO CHECK difference in values */
	BN_sub(difference, tamperedSignature ,signature);
	number_str = BN_bn2dec(difference);
	printf("Difference of value in the signed message (decimal) : %s \n", number_str);
	
	return 0;
}