#include <stdio.h>
#include <openssl/bn.h>
#include <stdlib.h>
#include <string.h>
#define SEPARATOR "\n================================\n"

void printBN(char *msg, BIGNUM * a){
	/* Use BN_bn2hex(a) for hex string
	* Use BN_bn2dec(a) for decimal string */
	char * number_str = BN_bn2hex(a);
	printf("%s %s\n", msg, number_str);

	OPENSSL_free(number_str);
}


int main ()
{
	// Define variables 
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *e = BN_new();
	BIGNUM *n = BN_new();
	BIGNUM *messageHex = BN_new();
	BIGNUM *tamperedSignature = BN_new();
	BIGNUM *signature = BN_new();
	BIGNUM *hexfromSignature = BN_new();
	BIGNUM *bnFromSignature = BN_new();
	BIGNUM *bnFromTamperedSignature = BN_new();
	BIGNUM *difference = BN_new();

	/* Convert the hex values into big number */
	BN_hex2bn(&messageHex, "4c61756e63682061206d697373696c652e");
	BN_hex2bn(&tamperedSignature, "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6803F");
	BN_hex2bn(&signature, "643D6F34902D9C7EC90CB0B2BCA36C47FA37165C0005CAB026C0542CBDB6802F");
	BN_hex2bn(&e,"010001");
	BN_hex2bn(&n, "AE1CD4DC432798D933779FBD46C6E1247F0CF1233595113AA51B450F18116115");

	/* calculate the signature */
	BN_mod_exp(bnFromSignature, signature, e, n, ctx);
	printBN("Hex signature:", bnFromSignature);

	/* calculate the tampered signatured */
	BN_mod_exp(bnFromTamperedSignature, tamperedSignature, e, n, ctx);
	printBN("Hex tampered signature:", bnFromTamperedSignature);



	if(BN_ucmp(bnFromTamperedSignature,bnFromSignature)==0) {
		printf( "%s The Signature is valid %s", SEPARATOR,SEPARATOR);
	}else {
		printf("%s The Signature is tampered! %s", SEPARATOR,SEPARATOR);

		/* Check the difference*/
		BN_sub(difference, bnFromTamperedSignature ,bnFromSignature);
		char * number_str =  BN_bn2dec(difference);
		printf("\n Difference of value between the signature (decimal) : %s \n", number_str);
		
	}
	
	return 0;
}