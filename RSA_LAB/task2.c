#include <stdio.h>
#include <openssl/bn.h>

void printBN(char *msg, BIGNUM * a){
	/* Use BN_bn2hex(a) for hex string
	* Use BN_bn2dec(a) for decimal string */
	char * number_str = BN_bn2hex(a);
	printf("%s %s\n", msg, number_str);

	OPENSSL_free(number_str);
}


int main ()
{
	/*Define Variables */
	BN_CTX *ctx = BN_CTX_new();
	BIGNUM *encryptedMessage = BN_new();
	BIGNUM *decryptedMessage = BN_new();
	BIGNUM *n = BN_new();
	BIGNUM *e = BN_new();
	BIGNUM *d = BN_new();
	BIGNUM *cipherText = BN_new();

	/* Convert the given primes from hex string into
	Big number*/

	// The message converted into hex 
	BN_hex2bn(&encryptedMessage, "4120746f702073656372657421");

	// Our n (modulus) value
	BN_hex2bn(&n, "DCBFFE3E51F62E09CE7032E2677A78946A849DC4CDDE3A4D0CB81629242FB1A5");
	
	// Our exponent value
	BN_hex2bn(&e, "010001");

	/* Use the formula of ciphertext = message ^ e mod n */
	BN_mod_exp(cipherText, encryptedMessage, e, n, ctx);
	printBN("The cipherText is: ", cipherText);

	// To check
	BN_hex2bn(&d, "74D806F9F3A62BAE331FFE3F0A68AFE35B3D2E4794148AACBC26AA381CD7D30D");
	BN_mod_exp(decryptedMessage, cipherText, d, n, ctx);
	printBN("The decrypted message is (in Hex): ", decryptedMessage );
	
	return 0;
}